package com.zzk.rxjavamvvm;

import com.zzk.rxjavamvvm.base.RXBaseApplication;
import com.zzk.rxjavamvvm.ui.error.ErrorActivity;
import com.zzk.rxjavamvvm.ui.login.LoginActivity;

import cat.ereza.customactivityoncrash.config.CaocConfig;

public class MyApplication extends RXBaseApplication {

    //##########################  custom variables start ##########################################

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    @Override
    public void onCreate() {
        super.onCreate();
        //初始化全局异常崩溃
        initCaocConfig();
    }

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    /**
     * rxmvvmbase的crash初始化
     */
    private void initCaocConfig() {
        CaocConfig.Builder.create()
                .backgroundMode(CaocConfig.BACKGROUND_MODE_SILENT) //背景模式,开启沉浸式
                .enabled(true) //是否启动全局异常捕获
                .showErrorDetails(true) //是否显示错误详细信息
                .showRestartButton(true) //是否显示重启按钮
                .trackActivities(true) //是否跟踪Activity
                .minTimeBetweenCrashesMs(2000) //崩溃的间隔时间(毫秒)
                .errorDrawable(R.mipmap.ic_launcher) //错误图标
                .restartActivity(LoginActivity.class) //重新启动后的activity
                .errorActivity(ErrorActivity.class) //崩溃后的错误activity，可以自定义，否则使用rxmvvmbaes中默认的
//                .eventListener(new YourCustomEventListener()) //崩溃后的错误监听
                .apply();
    }

    //##########################  custom metohds end   ############################################

    //##########################  custom variables start ##########################################

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    //##########################  custom metohds end   ############################################

}
