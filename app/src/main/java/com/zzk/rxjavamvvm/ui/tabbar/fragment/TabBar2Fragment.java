package com.zzk.rxjavamvvm.ui.tabbar.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.LogUtils;
import com.zzk.rxjavamvvm.BR;
import com.zzk.rxjavamvvm.R;
import com.zzk.rxjavamvvm.base.RXBaseFragment;

/**
 * @ProjectName: RXMVVMBase
 * @Package: com.zzk.rxmvvmbase
 * @ClassName:
 * @Description:
 * @Author: brilliantzhao
 * @CreateDate: 2021.1.13 10:12
 * @UpdateUser:
 * @UpdateDate: 2021.1.13 10:12
 * @UpdateRemark:
 * @Version: 1.0.0
 */
public class TabBar2Fragment extends RXBaseFragment {

    //##########################  custom variables start ##########################################

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    @Override
    public int initContentView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return R.layout.fragment_tab_bar_2;
    }

    @Override
    public int initVariableId() {
        return BR.viewModel;
    }

    @Override
    public void initData() {
        super.initData();
        LogUtils.i("TabBar2Fragment->initData");
    }

    @Override
    public void lazyLoadData() {
        LogUtils.i("TabBar2Fragment->lazyLoadData");
    }

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    //##########################  custom metohds end   ############################################

}
