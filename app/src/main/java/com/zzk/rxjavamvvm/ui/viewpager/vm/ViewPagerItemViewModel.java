package com.zzk.rxjavamvvm.ui.viewpager.vm;

import androidx.annotation.NonNull;

import com.zzk.rxmvvmbase.base.ItemViewModel;
import com.zzk.rxmvvmbase.binding.command.BindingAction;
import com.zzk.rxmvvmbase.binding.command.BindingCommand;

/**
 * 所有例子仅做参考,千万不要把它当成一种标准,毕竟主打的不是例子,业务场景繁多,理解如何使用才最重要。
 *
 * @ProjectName: RXMVVMBase
 * @Package: com.zzk.rxmvvmbase
 * @ClassName:
 * @Description:
 * @Author: brilliantzhao
 * @CreateDate: 2021.1.13 10:12
 * @UpdateUser:
 * @UpdateDate: 2021.1.13 10:12
 * @UpdateRemark:
 * @Version: 1.0.0
 */
public class ViewPagerItemViewModel extends ItemViewModel<ViewPagerViewModel> {

    //##########################  custom variables start ##########################################

    public String text;

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    public ViewPagerItemViewModel(@NonNull ViewPagerViewModel viewModel, String text) {
        super(viewModel);
        this.text = text;
    }

    public BindingCommand onItemClick = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            //点击之后将逻辑转到activity中处理
            viewModel.itemClickEvent.setValue(text);
        }
    });

    //##########################  custom metohds end   ############################################

}
