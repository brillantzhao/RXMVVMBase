package com.zzk.rxjavamvvm.ui.vpfrg;

import androidx.fragment.app.Fragment;

import com.zzk.rxjavamvvm.base.RXBasePagerFragment;
import com.zzk.rxjavamvvm.ui.tabbar.fragment.TabBar1Fragment;
import com.zzk.rxjavamvvm.ui.tabbar.fragment.TabBar2Fragment;
import com.zzk.rxjavamvvm.ui.tabbar.fragment.TabBar3Fragment;
import com.zzk.rxjavamvvm.ui.tabbar.fragment.TabBar4Fragment;

import java.util.ArrayList;
import java.util.List;

/**
 * ViewPager+Fragment的实现
 *
 * @ProjectName: RXMVVMBase
 * @Package: com.zzk.rxmvvmbase
 * @ClassName:
 * @Description:
 * @Author: brilliantzhao
 * @CreateDate: 2021.1.13 10:12
 * @UpdateUser:
 * @UpdateDate: 2021.1.13 10:12
 * @UpdateRemark:
 * @Version: 1.0.0
 */
public class ViewPagerGroupFragment extends RXBasePagerFragment {

    @Override
    protected List<Fragment> pagerFragment() {
        List<Fragment> list = new ArrayList<>();
        list.add(new TabBar1Fragment());
        list.add(new TabBar2Fragment());
        list.add(new TabBar3Fragment());
        list.add(new TabBar4Fragment());
        return list;
    }

    @Override
    protected List<String> pagerTitleString() {
        List<String> list = new ArrayList<>();
        list.add("page1");
        list.add("page2");
        list.add("page3");
        list.add("page4");
        return list;
    }
}
