package com.zzk.rxjavamvvm.ui.rvmulti;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ToastUtils;
import com.zzk.rxmvvmbase.base.BaseViewModel;
import com.zzk.rxmvvmbase.base.MultiItemViewModel;
import com.zzk.rxmvvmbase.binding.command.BindingAction;
import com.zzk.rxmvvmbase.binding.command.BindingCommand;

/**
 *
 * @ProjectName: RXMVVMBase
 * @Package: com.zzk.rxmvvmbase
 * @ClassName:
 * @Description:
 * @Author: brilliantzhao
 * @CreateDate: 2021.1.13 10:12
 * @UpdateUser:
 * @UpdateDate: 2021.1.13 10:12
 * @UpdateRemark:
 * @Version: 1.0.0
 */
public class MultiRecycleHeadViewModel extends MultiItemViewModel {

    public MultiRecycleHeadViewModel(@NonNull BaseViewModel viewModel) {
        super(viewModel);
    }

    //条目的点击事件
    public BindingCommand itemClick = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            ToastUtils.showShort("我是头布局");
        }
    });
}
