package com.zzk.rxjavamvvm.constant;

/**
 * @ProjectName: RXMVVMBase
 * @Package: com.zzk.rxmvvmbase.constant
 * @ClassName: AppConstant
 * @Description:
 * @Author: brilliantzhao
 * @CreateDate: 2021.1.13 11:04
 * @UpdateUser:
 * @UpdateDate: 2021.1.13 11:04
 * @UpdateRemark:
 * @Version: 1.0.0
 */
public class AppConstant {

    // 传递到LoadingDialogFragment的参数
    public static final String LOADING_DIALOG_FRAGMENT_TITLE = "LOADING_DIALOG_FRAGMENT_TITLE";

    // LoadingDialogFragment的tag标识
    public static final String LOADING_DIALOG_FRAGMENT_TAG = "LOADING_DIALOG_FRAGMENT_TAG";

}
