package com.zzk.rxjavamvvm.base;

import androidx.databinding.ViewDataBinding;

import com.zzk.rxjavamvvm.dialog.loading.LoadingDialogFragment;
import com.zzk.rxmvvmbase.base.BaseViewModel;
import com.zzk.rxmvvmbase.base.LazyBaseFragment;

import static com.zzk.rxjavamvvm.constant.AppConstant.LOADING_DIALOG_FRAGMENT_TAG;

/**
 * 进一步的封装，根据项目的需求实现自定义的公共基础类
 *
 * @ProjectName: RXMVVMBase
 * @Package: com.zzk.rxjavamvvm.base
 * @ClassName: RXBaseFragment
 * @Description:
 * @Author: brilliantzhao
 * @CreateDate: 2021.1.30 11:27
 * @UpdateUser:
 * @UpdateDate: 2021.1.30 11:27
 * @UpdateRemark:
 * @Version: 1.0.0
 */
public abstract class RXBaseFragment<VM extends BaseViewModel, DB extends ViewDataBinding> extends LazyBaseFragment<VM, DB> {

    //##########################  custom variables start ##########################################

    private LoadingDialogFragment loadingDialogFragment;

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    @Override
    public void lazyLoadData() {
    }

    /**
     * 避免在Base类中一直持有loadingDialogFragment引起对象无法释放，导致的内存泄漏
     */
    @Override
    public void showLoadingDialog(String title) {
        loadingDialogFragment = LoadingDialogFragment.newInstance(title, true);
        // 用isResumed来判断当前dialog是否正在展示,避免弹窗还没有消失的时候再次调用导致崩溃
        if (loadingDialogFragment.isResumed()) {
        } else {
            loadingDialogFragment.showNow(getChildFragmentManager(), LOADING_DIALOG_FRAGMENT_TAG);
        }
    }

    @Override
    public void dismissLoadingDialog() {
        if (loadingDialogFragment != null && loadingDialogFragment.isVisible()) {
            loadingDialogFragment.dismiss();
        }
    }

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    //##########################  custom metohds end   ############################################

}
