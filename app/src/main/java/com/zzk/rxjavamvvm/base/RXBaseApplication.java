package com.zzk.rxjavamvvm.base;

import com.zzk.rxjavamvvm.R;
import com.zzk.rxmvvmbase.base.BaseApplication;

/**
 * 进一步的封装，根据项目的需求实现自定义的公共基础类
 *
 * 添加该类的目的是在最精简BaseApplication的基础上封装一些和应用相关的初始化工作，最主要是满足组件化的需求，
 * 如果没有组件化的需要的话，可以去掉该层封装，直接放在最终的MyApplication中
 *
 * @ProjectName: RXMVVMBaseComponent
 * @Package: com.zzk.rxcomponent
 * @ClassName: HomeActivity
 * @Description:
 * @Author: brilliantzhao
 * @CreateDate: 2021.1.14 11:20
 * @UpdateUser:
 * @UpdateDate: 2021.1.14 11:20
 * @UpdateRemark:
 * @Version: 1.0.0
 */
public class RXBaseApplication extends BaseApplication {

    //##########################  custom variables start ##########################################

    private static RXBaseApplication sInstance;

    //
    public boolean isRelease = false;
    public boolean isShowLog = false;

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    @Override
    public void onCreate() {
        super.onCreate();
        //
        initGlobalInfo();
        // 调用父类的初始化方法
        initUtilCodeXLog(isShowLog);
    }

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    /**
     * 获得当前app运行的Application
     */
    public static RXBaseApplication getInstance() {
        if (sInstance == null) {
            synchronized (RXBaseApplication.class) {
                if (sInstance == null) {
                    sInstance = new RXBaseApplication();
                }
            }
        }
        return sInstance;
    }

    private void initGlobalInfo() {
        // 获取app当前是否是ONLINE状态，即正式的线上发布状态，此时需要关闭日志
        isRelease = getResources().getBoolean(R.bool.isRelease);
        // 判断日志是否展示
        isShowLog = getResources().getBoolean(R.bool.isShowLog);
    }

    //##########################  custom metohds end   ############################################
}
