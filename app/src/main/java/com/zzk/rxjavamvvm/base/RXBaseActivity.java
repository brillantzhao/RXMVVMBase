package com.zzk.rxjavamvvm.base;

import androidx.databinding.ViewDataBinding;

import com.zzk.rxjavamvvm.dialog.loading.LoadingDialogFragment;
import com.zzk.rxmvvmbase.base.BaseActivity;
import com.zzk.rxmvvmbase.base.BaseViewModel;

import static com.zzk.rxjavamvvm.constant.AppConstant.LOADING_DIALOG_FRAGMENT_TAG;

/**
 * 进一步的封装，根据项目的需求实现自定义的公共基础类
 *
 * @ProjectName: RXMVVMBase
 * @Package: com.zzk.rxjavamvvm.base
 * @ClassName: RXBaseActivity
 * @Description:
 * @Author: brilliantzhao
 * @CreateDate: 2021.1.30 11:27
 * @UpdateUser:
 * @UpdateDate: 2021.1.30 11:27
 * @UpdateRemark:
 * @Version: 1.0.0
 */
public abstract class RXBaseActivity<VM extends BaseViewModel, DB extends ViewDataBinding> extends BaseActivity<VM, DB> {

    //##########################  custom variables start ##########################################

    private LoadingDialogFragment loadingDialogFragment;

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    /**
     * 避免在Base类中一直持有loadingDialogFragment引起对象无法释放，导致的内存泄漏
     */
    @Override
    public void showLoadingDialog(String title) {
        loadingDialogFragment = LoadingDialogFragment.newInstance(title, true);
        /**
         *       public void show(FragmentManager manager, String tag){...}
         *       public int show(FragmentTransaction transaction, String tag){...}
         *
         * 第一种方式（FragmentManager）会开启新的FragmentTransaction，并且不将DialogFragment加入fragment回退栈
         * 第二种方式，因为用的是之前开启的FragmentTransaction，所以在FragmentTransaction提交时，接收了DialogFragment在回退栈中的标识符
         */
        /**
         * 1.show要比showNow稍微“慢”一点，这导致调用show了后，立刻修改dialog中的view（例如textView修改字符内容）会崩溃，而showNow不会
         * 2. 不可连续地调用show或者showNow 这个“连续”是指在弹窗还没有消失的时候再次调用 展示弹窗后fragment对象会添加到activity，而同一个fragment只能添加一次，所以连续调用会崩。
         */
        // 用isResumed来判断当前dialog是否正在展示,避免弹窗还没有消失的时候再次调用导致崩溃
        if (loadingDialogFragment.isResumed()) {

        } else {
            loadingDialogFragment.showNow(getSupportFragmentManager(), LOADING_DIALOG_FRAGMENT_TAG);
        }
    }

    @Override
    public void dismissLoadingDialog() {
        if (loadingDialogFragment != null && loadingDialogFragment.isVisible()) {
            loadingDialogFragment.dismiss();
        }
    }


    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    //##########################  custom metohds end   ############################################

}
