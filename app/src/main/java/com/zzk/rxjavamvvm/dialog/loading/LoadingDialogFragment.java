package com.zzk.rxjavamvvm.dialog.loading;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.zzk.rxjavamvvm.BR;
import com.zzk.rxjavamvvm.R;
import com.zzk.rxjavamvvm.constant.AppConstant;
import com.zzk.rxjavamvvm.databinding.FragmentLoadingDialogBinding;
import com.zzk.rxmvvmbase.base.BaseDialogFragment;

/**
 * @ProjectName: RXMVVMBase
 * @Package: com.zzk.rxmvvmbase
 * @ClassName:
 * @Description:
 * @Author: brilliantzhao
 * @CreateDate: 2021.1.13 10:12
 * @UpdateUser:
 * @UpdateDate: 2021.1.13 10:12
 * @UpdateRemark:
 * @Version: 1.0.0
 */
public class LoadingDialogFragment extends BaseDialogFragment<LoadingDialogViewModel, FragmentLoadingDialogBinding> {

    //##########################  custom variables start ##########################################

    private String title;

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    public static LoadingDialogFragment newInstance(String title) {
        LoadingDialogFragment fragment = new LoadingDialogFragment();
        fragment.setArguments(getBundleArguments(title));
        return fragment;
    }

    /**
     * 我们实例化自定义Fragment时，官方推荐Fragment.setArguments(Bundle bundle)这种方式来传递参数，而不推荐通过构造方法直接来传递参数
     *
     * @param title
     */
    public static LoadingDialogFragment newInstance(String title, boolean cancelable) {
        LoadingDialogFragment fragment = new LoadingDialogFragment();
        fragment.setArguments(getBundleArguments(title));
        fragment.setCancelable(cancelable);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 取出传过来的值
        Bundle bundle = getArguments();
        title = bundle.getString(AppConstant.LOADING_DIALOG_FRAGMENT_TITLE);
    }

    @Override
    public int initContentView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return R.layout.fragment_loading_dialog;
    }

    @Override
    public int initVariableId() {
        return BR.viewModel;
    }

    @Override
    public void initData() {
        super.initData();
        setLoadingTitle(title);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    /**
     * 设置传递参数
     *
     * @param title
     * @return
     */
    private static Bundle getBundleArguments(String title) {
        Bundle bundle = new Bundle();
        bundle.putString(AppConstant.LOADING_DIALOG_FRAGMENT_TITLE, title);
        return bundle;
    }

    /**
     * @param title
     */
    public void setLoadingTitle(String title) {
        viewModel.setLoadingTitle(title);
    }

    //##########################  custom metohds end   ############################################

}
