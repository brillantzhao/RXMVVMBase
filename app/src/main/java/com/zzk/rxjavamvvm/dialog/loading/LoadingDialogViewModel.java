package com.zzk.rxjavamvvm.dialog.loading;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.zzk.rxmvvmbase.base.BaseModel;
import com.zzk.rxmvvmbase.base.BaseViewModel;

/**
 * @ProjectName: RXMVVMBase
 * @Package: com.zzk.rxmvvmbase
 * @ClassName:
 * @Description:
 * @Author: brilliantzhao
 * @CreateDate: 2021.1.13 10:12
 * @UpdateUser:
 * @UpdateDate: 2021.1.13 10:12
 * @UpdateRemark:
 * @Version: 1.0.0
 */
public class LoadingDialogViewModel extends BaseViewModel {

    //##########################  custom variables start ##########################################

    // 绑定
    public ObservableField<String> loadingTitle = new ObservableField<>("");

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    public LoadingDialogViewModel(@NonNull Application application) {
        super(application);
    }

    public LoadingDialogViewModel(@NonNull Application application, BaseModel model) {
        super(application, model);
    }

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    /**
     * @param title
     */
    public void setLoadingTitle(String title) {
        loadingTitle.set(title);
    }


    //##########################  custom metohds end   ############################################
}
