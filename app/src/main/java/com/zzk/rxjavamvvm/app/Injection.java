package com.zzk.rxjavamvvm.app;

import com.zzk.rxjavamvvm.data.DemoRepository;
import com.zzk.rxjavamvvm.data.source.HttpDataSource;
import com.zzk.rxjavamvvm.data.source.LocalDataSource;
import com.zzk.rxjavamvvm.data.source.http.HttpDataSourceImpl;
import com.zzk.rxjavamvvm.data.source.http.service.DemoApiService;
import com.zzk.rxjavamvvm.data.source.local.LocalDataSourceImpl;
import com.zzk.rxjavamvvm.utils.RetrofitClient;

/**
 * 注入全局的数据仓库，可以考虑使用Dagger2。（根据项目实际情况搭建，千万不要为了架构而架构）
 *
 * @ProjectName: RXMVVMBase
 * @Package: com.zzk.rxmvvmbase
 * @ClassName:
 * @Description:
 * @Author: brilliantzhao
 * @CreateDate: 2021.1.13 10:12
 * @UpdateUser:
 * @UpdateDate: 2021.1.13 10:12
 * @UpdateRemark:
 * @Version: 1.0.0
 */
public class Injection {

    public static DemoRepository provideDemoRepository() {
        //网络API服务
        DemoApiService apiService = RetrofitClient.getInstance().create(DemoApiService.class);
        //网络数据源
        HttpDataSource httpDataSource = HttpDataSourceImpl.getInstance(apiService);
        //本地数据源
        LocalDataSource localDataSource = LocalDataSourceImpl.getInstance();
        //两条分支组成一个数据仓库
        return DemoRepository.getInstance(httpDataSource, localDataSource);
    }
}
