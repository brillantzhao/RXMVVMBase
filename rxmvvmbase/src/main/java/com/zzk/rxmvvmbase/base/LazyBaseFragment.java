package com.zzk.rxmvvmbase.base;

import androidx.databinding.ViewDataBinding;

/**
 * Androidx 模式下的懒加载
 * 以前处理 Fragment 的懒加载，我们通常会在 Fragment 中处理 setUserVisibleHint + onHiddenChanged 这两个函数，
 * 而在 Androidx 模式下，我们可以使用 FragmentTransaction.setMaxLifecycle() 的方式来处理 Fragment 的懒加载
 * <p>
 * 优点：在非特殊的情况下(缺点1)，只有实际的可见 Fragment，其 onResume 方法才会被调用，这样才符合方法设计的初衷。
 * 缺点：
 * 对于 Fragment 的嵌套，及时使用了 setMaxLifecycle 方法。同级不可见的Fragment， 仍然要调用 onResume 方法。
 * 需要在原有的 add+show+hide 方法中，继续调用 setMaxLifecycle 方法来控制Fragment 的最大生命状态。
 *
 * @ProjectName: RXMVVMBase
 * @Package: com.zzk.rxmvvmbase.base
 * @ClassName: LazyBaseFragment
 * @Description:
 * @Author: brilliantzhao
 * @CreateDate: 2021.1.29 13:35
 * @UpdateUser:
 * @UpdateDate: 2021.1.29 13:35
 * @UpdateRemark:
 * @Version: 1.0.0
 */
public abstract class LazyBaseFragment<VM extends BaseViewModel, DB extends ViewDataBinding> extends BaseFragment<VM, DB> {

    //##########################  custom variables start ##########################################

    private boolean isLoaded = false;

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    @Override
    public void onResume() {
        super.onResume();
        if (!isLoaded && !isHidden()) {
            lazyLoadData();
            isLoaded = true;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isLoaded = false;
    }

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    /**
     * 懒加载
     */
    public abstract void lazyLoadData();

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    //##########################  custom metohds end   ############################################

}
