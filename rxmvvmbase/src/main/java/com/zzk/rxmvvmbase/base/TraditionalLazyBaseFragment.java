package com.zzk.rxmvvmbase.base;

import androidx.databinding.ViewDataBinding;

/**
 * 传统方式的懒加载
 * 以前处理 Fragment 的懒加载，我们通常会在 Fragment 中处理 setUserVisibleHint + onHiddenChanged 这两个函数，
 * 而在 Androidx 模式下，我们可以使用 FragmentTransaction.setMaxLifecycle() 的方式来处理 Fragment 的懒加载
 *
 * @ProjectName: RXMVVMBase
 * @Package: com.zzk.rxmvvmbase.base
 * @ClassName: TraditionalLazyBaseFragment
 * @Description:
 * @Author: brilliantzhao
 * @CreateDate: 2021.1.29 14:06
 * @UpdateUser:
 * @UpdateDate: 2021.1.29 14:06
 * @UpdateRemark:
 * @Version: 1.0.0
 */
public abstract class TraditionalLazyBaseFragment<VM extends BaseViewModel, DB extends ViewDataBinding> extends BaseFragment<VM, DB> {

    //##########################  custom variables start ##########################################

    /**
     * 是否执行懒加载
     */
    private boolean isLoaded = false;

    /**
     * 当前Fragment是否对用户可见
     */
    private boolean isVisibleToUser = false;

    /**
     * 因为当使用ViewPager+Fragment形式会调用该方法时，setUserVisibleHint会优先Fragment生命周期函数调用，
     * 所以这个时候就,会导致在setUserVisibleHint方法执行时就执行了懒加载，
     * 而不是在onResume方法实际调用的时候执行懒加载。所以需要这个变量
     */
    private boolean isCallResume = false;

    /**
     * 是否调用了setUserVisibleHint方法。处理show+add+hide模式下，默认可见 Fragment 不调用
     * onHiddenChanged 方法，进而不执行懒加载方法的问题。
     */
    private boolean isCallUserVisibleHint = false;

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    @Override
    public void onResume() {
        super.onResume();
        isCallResume = true;
        if (!isCallUserVisibleHint) isVisibleToUser = !isHidden();
        judgeLazyInit();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isLoaded = false;
        isVisibleToUser = false;
        isCallUserVisibleHint = false;
        isCallResume = false;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        this.isVisibleToUser = isVisibleToUser;
        isCallUserVisibleHint = true;
        judgeLazyInit();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        isVisibleToUser = !hidden;
        judgeLazyInit();
    }

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    /**
     * 懒加载
     */
    public abstract void lazyLoadData();

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    private void judgeLazyInit() {
        if (!isLoaded && isVisibleToUser && isCallResume) {
            lazyLoadData();
            isLoaded = true;
        }
    }

    //##########################  custom metohds end   ############################################

}
