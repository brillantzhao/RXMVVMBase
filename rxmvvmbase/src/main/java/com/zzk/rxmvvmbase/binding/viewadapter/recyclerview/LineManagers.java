package com.zzk.rxmvvmbase.binding.viewadapter.recyclerview;

import androidx.recyclerview.widget.RecyclerView;

/**
 *
 * @ProjectName: RXMVVMBase
 * @Package: com.zzk.rxmvvmbase
 * @ClassName:
 * @Description:
 * @Author: brilliantzhao
 * @CreateDate: 2021.1.13 10:12
 * @UpdateUser:
 * @UpdateDate: 2021.1.13 10:12
 * @UpdateRemark:
 * @Version: 1.0.0
 */
public class LineManagers {

    protected LineManagers() {
    }

    /**
     *
     */
    public interface LineManagerFactory {
        RecyclerView.ItemDecoration create(RecyclerView recyclerView);
    }

    /**
     * @return
     */
    public static LineManagerFactory both() {
        return new LineManagerFactory() {
            @Override
            public RecyclerView.ItemDecoration create(RecyclerView recyclerView) {
                return new DividerLine(recyclerView.getContext(), DividerLine.LineDrawMode.BOTH);
            }
        };
    }

    /**
     * @return
     */
    public static LineManagerFactory horizontal() {
        return new LineManagerFactory() {
            @Override
            public RecyclerView.ItemDecoration create(RecyclerView recyclerView) {
                return new DividerLine(recyclerView.getContext(), DividerLine.LineDrawMode.HORIZONTAL);
            }
        };
    }

    /**
     * @param dividerDPSize
     * @return
     */
    public static LineManagerFactory horizontal(int dividerDPSize) {
        return new LineManagerFactory() {
            @Override
            public RecyclerView.ItemDecoration create(RecyclerView recyclerView) {
                return new DividerLine(recyclerView.getContext(), dividerDPSize, DividerLine.LineDrawMode.HORIZONTAL);
            }
        };
    }

    /**
     * @return
     */
    public static LineManagerFactory vertical() {
        return new LineManagerFactory() {
            @Override
            public RecyclerView.ItemDecoration create(RecyclerView recyclerView) {
                return new DividerLine(recyclerView.getContext(), DividerLine.LineDrawMode.VERTICAL);
            }
        };
    }

    /**
     * 带有定制divider尺寸的
     * 这里的绘制分割线有问题
     *
     * @param dividerDPSize
     * @return
     */
    public static LineManagerFactory vertical(int dividerDPSize) {
        return new LineManagerFactory() {
            @Override
            public RecyclerView.ItemDecoration create(RecyclerView recyclerView) {
                return new DividerLine(recyclerView.getContext(), dividerDPSize, DividerLine.LineDrawMode.VERTICAL);
            }
        };
    }
}
