package com.zzk.rxmvvmbase.binding.command;

/**
 * Represents a function with zero arguments.
 *
 * @param <T> the result type
 * @ProjectName: RXMVVMBase
 * @Package: com.zzk.rxmvvmbase
 * @ClassName:
 * @Description:
 * @Author: brilliantzhao
 * @CreateDate: 2021.1.13 10:12
 * @UpdateUser:
 * @UpdateDate: 2021.1.13 10:12
 * @UpdateRemark:
 * @Version: 1.0.0
 */
public interface BindingFunction<T> {
    T call();
}
