package com.zzk.rxmvvmbase.http.interceptor.logging;

/**
 * @ProjectName: RXMVVMBase
 * @Package: com.zzk.rxmvvmbase
 * @ClassName:
 * @Description:
 * @Author: brilliantzhao
 * @CreateDate: 2021.1.13 10:12
 * @UpdateUser:
 * @UpdateDate: 2021.1.13 10:12
 * @UpdateRemark:
 * @Version: 1.0.0
 */
public enum Level {
    /**
     * No logs.
     */
    NONE,
    /**
     * <p>Example:
     * <pre>{@code
     *  - URL
     *  - Method
     *  - Headers
     *  - Body
     * }</pre>
     */
    BASIC,
    /**
     * <p>Example:
     * <pre>{@code
     *  - URL
     *  - Method
     *  - Headers
     * }</pre>
     */
    HEADERS,
    /**
     * <p>Example:
     * <pre>{@code
     *  - URL
     *  - Method
     *  - Body
     * }</pre>
     */
    BODY
}
